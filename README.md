Задание к лабораторной работе №1
по дисциплине «Системы ввода/вывода»
       Авторы:Ло Аонань Дкн Яжу
                  Группа:  P3304C
                           Вариант 2
Название： “Разработкадрайверовсимвольныхустройств”
Цель работы: получить знания и навыки разработки драйверов символьных устройств для операционной системы Linux.
При записи в файл символьного устройства текста типа “5+6” должен запоминаться результат операции, то есть 11 для данного примера. Должны поддерживаться операции сложения, вычитания, умножения и деления. Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла /proc/varN.
Описаниефункциональностидрайвера：
При записи в файл символьного устройства текста типа “5+6” должен запоминаться результат операции, то есть 11 для данного примера. Должны поддерживаться операции сложения, вычитания, умножения и деления. Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении созданного файла /proc/varN в консоль пользователя.
При чтении из файла символьного устройства в кольцевой буфер ядра должен осуществляться вывод тех же данных, которые выводятся при чтении файла /proc/varN.
Инструкцияпосборке：
Open the terminal and enter the var1 folder, run the make command, the proc_example2.ko module file will be generated

After running sudo insmod proc_example2.ko to load the module,

Run the dmesg command to view the dynamically generated device number. There are two device numbers, the major device number and the minor device number.
Run sudo mknod /dev/var2 c 242 0 to create a character device. You need to change the last two digits of the command according to your output.

Among them, test2.sh and test3.sh are script programs used for testing, which need to be run with the sudo bash ./test2.sh command.

test2.sh and test3.sh are used to write data to the /proc/var2 and /dev/var2 devices, respectively.

After running, the program will calculate the value of the expression. You can run the script multiple times, and the kernel module will record the result of each expression calculation.
Running sudo cat /proc/var2 or sudo cat /dev/var2 will output the same sequence of characters.
